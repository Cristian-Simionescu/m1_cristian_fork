import nltk
nltk.download("punkt")
nltk.download("averaged_perceptron_tagger")
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import wordnet

example1 = "Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause."

example2 = '''Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause.
Risk factors include being overweight or a recent increase in weight. Tetracycline may also trigger the condition. The diagnosis is based on symptoms and a high intracranial pressure founding during a lumbar puncture with no specific cause found on a brain scan.'''

example3 = '''Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause. The main symptoms are headache, vision problems, ringing in the ears with the heartbeat, and shoulder pain. Complications may include vision loss.
Risk factors include being overweight or a recent increase in weight. Tetracycline may also trigger the condition. The diagnosis is based on symptoms and a high intracranial pressure founding during a lumbar puncture with no specific cause found on a brain scan.
Treatment includes a healthy diet, salt restriction, and exercise. Bariatric surgery may also be used to help with weight loss. The medication acetazolamide may also be used along with the above measures. A small percentage of people may require surgery to relieve the pressure.
About 2 per 100,000 people are newly affected per year. The condition most commonly affects women aged 20–50. Women are affected about 20 times more often than men. The condition was first described in 1897.'''

def separate_by_paragraphs(text):
    '''This function will receive as input raw text\nand give as output a list of all paragraphs found.'''
    return text.split("\n")

def _tokenize_paragraph(paragraph):
    '''This function will receive as input a paragraph\nand give as output a list of all sentences found.'''
    return sent_tokenize(paragraph)

def _tokenize_sentence(sentence):
    '''This function will receive as input a sentence\nand give as output a list of all tokens found.'''
    return word_tokenize(sentence)

def stopWords(test):
    '''stopWords function receives a list as input and returns a list without punctuation marks and stopWords'''

    stopWords= set (stopwords.words("english"))
    #print(stopWords)

    newTest= ["".join( j for j in i if j not in string.punctuation) for i in  test]

    newTest = [s for s in newTest if s]

    finalScript=[]

    for word in newTest:
        if word not in stopWords:
            finalScript.append(word)


    return finalScript

def punctMarksOnly(test):
    ''' punctMarksOnly function receives a list as input and returns a list without punctuation marks '''

    newTest = ["".join(j for j in i if j not in string.punctuation) for i in test]

    newTest = [s for s in newTest if s]

    return newTest

def stopWordsOnly(test):
    ''' stopWordsOnly function receives a list as input and returns a list without stopWords'''

    stopWords = set(stopwords.words("english"))
    # print(stopWords)

    finalScript = []

    for word in test:
        if word not in stopWords:
            finalScript.append(word)

    return finalScript

def posTagger(tokenized):
    '''The posTagger function take as paramerer a list of tokenized words and returns a list consisting of pairs word - part of speech tag'''
    try:
        for i in tokenized:
            words = _tokenize_sentence(i)
            tagged = nltk.pos_tag(words)
            return tagged

    except Exception as e:
        print(str(e))

def process_content(tokenized):
	'''The process_content function takes as a parameter a list of tokenized words and recognises\neither a general named entity, or it can recognize names, locations, monetary amounts, dates...'''

	try:
		for i in tokenized:
			words = nltk.word_tokenize(i)
			tagged = nltk.pos_tag(words)

			namedEnt = nltk.ne_chunk(tagged)

			namedEnt.draw()

	except Exception as e:
		print(str(e))

def _stemm(text):
    '''This function will receive as input raw text\nand give as output a list of unique stemms found.'''
    ps = PorterStemmer()
    words = _tokenize_sentence(text)
    result = set()
    for w in words:
        result.add(ps.stem(w))
    return list(result)

def has_hypernym(word1, word2):
    '''This function will receive as input two words\nand query wordnet to find whether the second word\nis a hypernym for the first word:\ntrue : if they are\nfalse: otherwise.'''

    def extract_word_from_synset(synset):
        '''This function will receive as input a synset and give as output the word of that synset'''
        return synset.name().split(".")[0]

    synArray = wordnet.synsets(word1)
    main_synset = synArray[0]

    for hypernym in main_synset.hypernyms():
        if extract_word_from_synset(hypernym) == word2:
            return True

    return False

def get_hypernym_relations(words):
    '''This function will receive as input a list of words\nand give as output a dictionary.\nEach key represents a word.\nEach value represents a list of higher concepts for\nthe corresponding key\n(these concepts can also be found in the input list).'''
    relations = dict()
    for i in range(len(words)):
        for j in range(len(words)):
            if i is not j:
                if has_hypernym(words[i], words[j]):
                    if words[i] in relations:
                        relations[words[i]].append(words[j])
                    else:
                        relations[words[i]] = [words[j]]
    return relations

def prettify(elem):
    '''Return a pretty-printed XML string for the Element.'''

    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def format_XML(text):
    '''Service which integrates every function in the module'''

    preprocessed = Element('PREPROCESSED')

    paragraphs = separate_by_paragraphs(text)

    paragraphs_counter = 1
    sentences_counter = 1
    words_counter = 1

    for paragraph in paragraphs:
        para = SubElement(preprocessed, 'PARA', {'ID': 'PARA' + str(paragraphs_counter)})

        sentences = _tokenize_paragraph(paragraph)

        for sentence in sentences:
            sent = SubElement(para, 'SENT', {'ID': 'SENT' + str(sentences_counter)})

            words = _tokenize_sentence(sentence)

            pos_tags = posTagger([sentence])

            for idx, word in enumerate(words):
                w = SubElement(sent, 'W', {'ID': 'W' + str(words_counter), \
                                           'POS': pos_tags[idx][1]})

                w.text = word

                words_counter += 1

            sentences_counter += 1

        paragraphs_counter += 1

    return prettify(preprocessed)

def main(f):
    return format_XML(f)

if __name__ == "__main__":
    print((format_XML(example3)))
